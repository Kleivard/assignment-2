
// HTML paragraphs for currencys (bank, salory, loan).
const balanceParaElement = document.getElementById("balance")
const payParaElement = document.getElementById("payPara")
const loanParaElement = document.getElementById("loan")

// HTML select for laptops (gathered from API).
const laptopSelectElement = document.getElementById("laptopSelect")

// HTML elements for laptop information (price, title, image, features, description).
const laptopPriceHeaderElement = document.getElementById("computerPrice")
const laptopFeatParaElement = document.getElementById("featPara")
const laptopImageElement = document.getElementById("laptopImg")
const laptopTitleHeaderElement = document.getElementById("computerName")
const laptopDesciptionParaElement = document.getElementById("computerInfoPara")

// HTML buttons for loan, bank, work, buy-laptop and pay loan.
const loanButtonElement = document.getElementById("loanButton")
const bankButtonElement = document.getElementById("bankButton")
const workButtonElement = document.getElementById("workButton")
const buyButtonElement = document.getElementById("buyButton")
const payLoanButtonElement = document.getElementById("payLoanButton")

// Internal variables used to control the currency.
let bankBalance = 0
let payBalance = 0
let loanBalance = 0
let totalBalance = 0
// Stores the laptops from the API.
let laptops = []

// Fetches the laptops from the API and inserts them into an array. Init first computer html elements.
try {
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToSelect(laptops))
} catch (error) {
    console.log("Error fetching laptop data: " + error.message)
}

// Loop the API call.
const addLaptopsToSelect = (laptops) => {
    laptops.forEach(laptop => addLaptopoSelect(laptop))
    // Quickfix for incorrect extention (jpg to png)
    laptops[4].image = "assets/images/5.png"
    onInitialize()
}
// Creates a new option element for each laptop in the API showing the title.
const addLaptopoSelect = (laptop) => {
    const laptopOptionElement = document.createElement("option")
    laptopOptionElement.value = laptop.id;
    laptopOptionElement.appendChild(document.createTextNode(laptop.title))
    laptopSelectElement.appendChild(laptopOptionElement)
}
// Changes HTML element information for when computer is changed in drop-down menu.
const updateComputerSpecs = (element) => {      
    const selectedLaptop = laptops[element.target.selectedIndex]
    laptopFeatParaElement.innerText = selectedLaptop.specs
    laptopTitleHeaderElement.innerText = selectedLaptop.title
    laptopDesciptionParaElement.innerText = selectedLaptop.description        
    laptopPriceHeaderElement.value = selectedLaptop.price
    laptopPriceHeaderElement.innerText = selectedLaptop.price + " SEK"                                                                                                        
    laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image                                                             

}
// Handels Get a loan button functionality 
const handleLoanButton = () => {
    // prompt user from amount, check if int else reprompt
    const amountToLoan = (function ask(){
        let input = prompt("Please insert the amount you want to loan (SEK)")
        return isNaN(input) || input <= 0 ? ask() : input
    }());
    //  checks lan conditions and unhides the loan elements
        if (checkLoanConditions(amountToLoan)){
            loanBalance = amountToLoan
            loanParaElement.innerText = "Loan: " + loanBalance + " kr"
            totalBalance = parseInt(bankBalance) + parseInt(loanBalance)
            balanceParaElement.innerText = "Balance: " + totalBalance + " kr"
            loanParaElement.style.display = "block"
            payLoanButtonElement.style.display = "inline-block"
        }
}
// checks if the loan is less then twice the bank balance. Prompt accordingly 
const checkLoanConditions = (loanAmount) => {
    if((bankBalance * 2) < loanAmount){
        alert(`A bit greedy now ? Max amount to loan is ${bankBalance * 2} `)
        return false
    }else if(loanBalance > 0){
        alert("Pay off the loan you have first... GET TO WORK")
        return false
    }else return true
}
// Transfer salory to bankblance. 
const handleBankkButton = () => {
    checkOutstandingLoan()
    bankBalance += payBalance
    payBalance = 0
    totalBalance = parseInt(bankBalance) + parseInt(loanBalance)
    payParaElement.innerText = "Salory: " + payBalance + " kr"
    balanceParaElement.innerText = "Balance: " + totalBalance + " kr"
}
// If 10% of pay is less then the users loan it is reduced from the loan and rest goes to bank balance
const checkOutstandingLoan = () =>{
    if(loanBalance > 0 && loanBalance > (payBalance* 0.1)){
        loanBalance -= payBalance* 0.1
        payBalance = payBalance* 0.9
        loanParaElement.innerText = "Loan: " + loanBalance + " kr"
    }
}
// Adds 100 to salory each click
const handleWorkButton = () => {
    payBalance += 100
    payParaElement.innerText = "Salory: " + payBalance + " kr"
}
// Pays of loan
const handlePayLoanButton = () => {
    if(payBalance <= 0){
        alert("Need money to pay off the loan")
    }else{
        // Checks if salory is graeter then the loan
        let remaining = loanBalance - payBalance
        if(remaining > 0){
            // Salory is less then loan: Remove salory from loan and resets the current salory
            loanBalance -= payBalance
            payBalance = 0
            payParaElement.innerText = "Salory: " + payBalance + " kr"
            loanParaElement.innerText = "Loan: " + loanBalance + " kr"
            totalBalance = parseInt(bankBalance) + parseInt(loanBalance)
            balanceParaElement.innerText = "Balance: " + totalBalance + " kr"
            
        }else{
            // Salory is greater then loan: Remove salory from loan and resets the current loan and transfers the remaining amout to bankblaance
            bankBalance += payBalance - loanBalance
            payBalance = 0
            loanBalance = 0
            loanParaElement.style.display = "none"
            payLoanButtonElement.style.display = "none"
            payParaElement.innerText = "Salory: " + payBalance + " kr"
            totalBalance = parseInt(bankBalance) + parseInt(loanBalance)
            balanceParaElement.innerText = "Balance: " + totalBalance + " kr"
        }
    }   
}
// Prompt user with new computer bought
const handleBuyButton = () => {
    const laptopInfo = {
        price: laptopPriceHeaderElement.value,
        name: laptopTitleHeaderElement.innerText
   }
   // Checks if bankbalance + loan is grater or equal to laptop price
    if(bankBalance+loanBalance > laptopInfo.price){
        bankBalance -= laptopInfo.price
        totalBalance = parseInt(bankBalance) + parseInt(loanBalance)
        balanceParaElement.innerText = "Balance: " + totalBalance + " kr"
        alert("Gratz on the new " +laptopInfo.name + " mate !")
    }else{
        alert("You don't have enough money sorry bro")
    }


}
// Init laptop information to html
const onInitialize = () => {
    laptopFeatParaElement.innerText = laptops[0].specs
    laptopTitleHeaderElement.innerText = laptops[0].title
    laptopDesciptionParaElement.innerText = laptops[0].description   
    laptopPriceHeaderElement.value = laptops[0].price   
    laptopPriceHeaderElement.innerText = laptops[0].price + " SEK"                                                                                                          
    laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image
}
// Adding event listeners to HTML buttons
payLoanButtonElement.addEventListener("click", handlePayLoanButton)
loanButtonElement.addEventListener("click", handleLoanButton)
bankButtonElement.addEventListener("click", handleBankkButton)
workButtonElement.addEventListener("click", handleWorkButton)
buyButtonElement.addEventListener("click", handleBuyButton)
laptopSelectElement.addEventListener("change", updateComputerSpecs)